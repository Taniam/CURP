/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package curp;

public class CURP {
    /**
     * Metodo que regresa el primer caracter del nombre (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param nombre Nombre(s) del usuario.
     * @return El primer caracter del nombre.
     */
    String nombre(String nombre){
        nombre=nombre.toUpperCase().trim().
                replace("Á","A").replace("É","E").replace("Í", "I").
                replace("Ó","O").replace("Ú", "U").replace("Ñ","X");
        nombre = nombre.substring(0, 1);
        
        return (nombre);
    }
    
    /**
     * Metodo que regresa los primeros dos caracteres del apellido paterno (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param apellido_p El apellido paterno del usuario.
     * @return Los dos primero caracteres del apellido paterno.
     */
    String apellidoPaterno(String apellido_p){
        apellido_p=apellido_p.toUpperCase().trim().
                replace("Á","A").replace("É","E").replace("Í", "I").
                replace("Ó","O").replace("Ú", "U").replace("Ñ","X");
        apellido_p=apellido_p.substring(0, 2);
            
        return (apellido_p);
    }
    
    /**
     * Metodo que regresa el primer caracter del apellido materno (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param apellido_m El apellido materno del usuario.
     * @return El primer caracter del apellido materno.
     */
    String apellidoMaterno(String apellido_m){
        apellido_m=apellido_m.toUpperCase().trim().
                replace("Á","A").replace("É","E").replace("Í", "I").
                replace("Ó","O").replace("Ú", "U").replace("Ñ","X");
        apellido_m=apellido_m.substring(0, 1);
        
        return (apellido_m);
    }
    
    /**
     * Metodo que regresa el mes en formato: mm.
     * El metodo debe de aceptar el nombre del mes sin importar si vienen en minusculas o
     * mayusculas o mixto, tambien debe de aceptar si el mes fue dado como un entero.
     * Ejemplos de posibles entradas:
     * - Enero
     * - enero
     * - EnErO
     * - 01
     * - 1
     * Para cualquiera de las entradas anteriores la cadena que debe regresar es: 01
     * @param mes El mes dado.
     * @return El mes en formato: mm.
     */
    String getMes(String mes){
        mes=mes.toUpperCase().trim();
        
        if(mes.equals("ENERO")){
            mes="01";
            return(mes);
        }
        if(mes.equals("FEBRERO")){
            mes="02";
            return(mes);
        }
        if(mes.equals("MARZO")){
            mes="03";
            return(mes);
        }
        if(mes.equals("ABRIL")){
            mes="04";
            return(mes);
        }
        if(mes.equals("MAYO")){
            mes="05";
            return(mes);
        }
        if(mes.equals("JUNIO")){
            mes="06";
            return(mes);
        }
        if(mes.equals("JULIO")){
            mes="07";
            return(mes);
        }
        if(mes.equals("AGOSTO")){
            mes="08";
            return(mes);
        }
        if(mes.equals("SEPTIEMBRE")){
            mes="09";
            return(mes);
        }
        if(mes.equals("OCTUBRE")){
            mes="10";
            return(mes);
        }
        if(mes.equals("NOVIEMBRE")){
            mes="11";
            return(mes);
        }
        if(mes.equals("DICIEMBRE")){
            mes="12";
            return(mes);
        }
        if(mes.equals("1")){
            mes="01";
            return(mes);
        }
        if(mes.equals("2")){
            mes="02";
            return(mes);
        }
        if(mes.equals("3")){
            mes="03";
            return(mes);
        }
        if(mes.equals("4")){
            mes="04";
            return(mes);
        }
        if(mes.equals("5")){
            mes="05";
            return(mes);
        }
        if(mes.equals("6")){
            mes="06";
            return(mes);
        }
        if(mes.equals("7")){
            mes="07";
            return(mes);
        }
        if(mes.equals("8")){
            mes="08";
            return(mes);
        }
        if(mes.equals("9")){
            mes="09";
            return(mes);
        }
        else{
            return(mes);
        }
        
    }
    
    /**
     * Metodo que regresa la fecha de nacimiento en el formato correcto: aammdd
     * La fecha viene en el siguiente formato: dd/mm/aaaa
     * Hint: Usen el metodo split para obtener los datos.
     * @param fecha La fecha de nacimiento del usuario.
     * @return La fecha en formato: aammdd.
     */
    String fechaNacimiento(String fecha){         
        String [] arreglo = fecha.split("/");
        
        if(arreglo[0].equals("1")){
            arreglo[0]="01";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        if(arreglo[0].equals("2")){
            arreglo[0]="02";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        if(arreglo[0].equals("3")){
            arreglo[0]="03";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        if(arreglo[0].equals("4")){
            arreglo[0]="04";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        if(arreglo[0].equals("5")){
            arreglo[0]="05";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        if(arreglo[0].equals("6")){
            arreglo[0]="06";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        if(arreglo[0].equals("7")){
            arreglo[0]="07";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        if(arreglo[0].equals("8")){
            arreglo[0]="08";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        if(arreglo[0].equals("9")){
            arreglo[0]="09";
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return(fecha1);
        }
        else{
            String fecha1= arreglo[2].substring(2)+getMes(arreglo[1])+arreglo[0];
            return (fecha1);
        }
    }
    
    /**
     * Metodo que regresa el primer caracter del sexo dado, en el formato correcto.
     * @param sexo El sexo del usuario.
     * @return El primer caracter del sexo.
     */
    String sexo(String sexo){
        
        sexo=sexo.toUpperCase().trim().substring(0, 1);
        
        return (sexo);
    }
    
    /**
     * Metodo que regresa la abreviatura del estado, en el formato correcto.
     * El metodo debe de aceptar entradas en minusculas, mayusculas o mixta,
     * tambien debe de aceptar entradas con acentos y sin acentos.
     * @param estado El estado de nacimiento del usuario.
     * @return La abreviatura del usuario.
     */
    String estado(String estado){
        String estado1=estado.toUpperCase().replace("Á","A").replace("É","E").replace("Í", "I").
                replace("Ó","O").replace("Ú", "U").replace("Ñ","X");
        
            if(estado1.equals("AGUAS CALIENTES")){
                estado="AS";
                return (estado);
            }
            if(estado1.equals("CIUDAD DE MEXICO")){
                estado="DF";
                return(estado);
            }
            if(estado1.equals("BAJA CALIFORNIA")){
                estado="BS";
                return (estado);
            }
            if(estado1.equals("COAHUILA")){
                estado="CL";
                return (estado);
            }
            if(estado1.equals("CHIAPAS")){
                estado="CS";
                return (estado);
            }
            if(estado1.equals("DISTRITO FEDERAL")){
                estado="DF";
                return (estado);
            }
            if(estado1.equals("GUANAJUATO")){
                estado="GI";
                return (estado);
            }
            if(estado1.equals("HIDALGO")){
                estado="HG";
                return (estado);
            }
            if(estado1.equals("MEXICO")){
                estado="MC";
                return (estado);
            }
            if(estado1.equals("MORELOS")){
                estado="MS";
                return (estado);
            }
            if(estado1.equals("NUEVO LEON")){
                estado="NL";
                return (estado);
            }
            if(estado1.equals("AGUAS CALIENTES")){
                estado="AS";
                return (estado);
            }
            if(estado1.equals("PUEBLA")){
                estado="PL";
                return (estado);
            }
            if(estado1.equals("QUINTANA ROO")){
                estado="QR";
                return (estado);
            }
            if(estado1.equals("SINALOA")){
                estado="SL";
                return (estado);
            }
            if(estado1.equals("TABASCO")){
                estado="TC";
                return (estado);
            }
            if(estado1.equals("TLAXCALA")){
                estado="TL";
                return (estado);
            }
            if(estado1.equals("YUCATAN")){
                estado="YN";
                return (estado);
            }
            if(estado1.equals("NACIDO EN EL EXTRANJERO")){
                estado="NE";
                return (estado);
            }
            if(estado1.equals("BAJA CALIFORNIA")){
                estado="BC";
                return (estado);
            }
            if(estado1.equals("CAMPECHE")){
                estado="CC";
                return (estado);
            }
            if(estado1.equals("COLIMA")){
                estado="CM";
                return (estado);
            }
            if(estado1.equals("CHIHUAHUA")){
                estado="CH";
                return (estado);
            }
            if(estado1.equals("DURANGO")){
                estado="DG";
                return (estado);
            }
            if(estado1.equals("GUERRERO")){
                estado="GR";
                return (estado);
            }
            if(estado1.equals("JALISCO")){
                estado="JC";
                return (estado);
            }
            if(estado1.equals("MICHOACAN")){
                estado="MN";
                return (estado);
            }
            if(estado1.equals("NAYARIT")){
                estado="NT";
                return (estado);
            }
            if(estado1.equals("OAXACA")){
                estado="OC";
                return (estado);
            }
            if(estado1.equals("QUERETARO")){
                estado="QR";
                return (estado);
            }
            if(estado1.equals("SAN LUIS POTOSI")){
                estado="SP";
                return (estado);
            }
            if(estado1.equals("SONORA")){
                estado="SR";
                return (estado);
            }
            if(estado1.equals("TAMAULIPAS")){
                estado="TS";
                return (estado);
            }
            if(estado1.equals("VERACRUZ")){
                estado="VZ";
                return (estado);
            }
            if(estado1.equals("ZACATECAS")){
                estado="ZS";
                return (estado);
            }
            else{
                return (estado);
            }
    }
    
    /**
     * Metodo que regresa la segunda consonante de una cadena.
     * Hint: remplacen las vocales con acentos por vocales sin acentos y
     * pasen toda la cadena a minusculas o mayusculas para que tengan menos casos que revisar.
     * @param cadena La cadena dada.
     * @return La segunda consonante de una cadena.
     */
    char getConsonantes(String cadena){
        cadena=cadena.toUpperCase().trim();
        cadena=cadena.replace("Á","A").replace("É","E").replace("Í", "I").
                replace("Ó","O").replace("Ú", "U").replace("Ñ","X");
        String cadena1= "";
        
        for(int i=0; i<cadena.length(); i++){
            if(cadena.charAt(i)!='A' && cadena.charAt(i)!='E' && cadena.charAt(i)!='I' &&
                    cadena.charAt(i)!='O' && cadena.charAt(i)!= 'U'){
                cadena1= cadena1 +cadena.charAt(i);
            }
        }
        return cadena1.charAt(1);
    }
}